from fabric.api import run, local, lcd

def prepare_deploy():
    # with lcd('./mappingsite'):
    #     local("./manage.py test storemapapp")
    with lcd('./storedirectoryscraper/'):
        local('python -m unittest storedirectoryscraper.tests')
    local("git add -p && git commit")
    local("git push")

from scrapy_djangoitem import DjangoItem
from storemapapp.models import Office


class OfficeItem(DjangoItem):
    django_model = Office

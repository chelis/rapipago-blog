# -*- coding: utf-8 -*-

# Scrapy settings for storedirectoryscraper project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html

import sys
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.append(os.path.join(BASE_DIR, '../mappingsite'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'mappingsite.settings'

import django
django.setup ()


BOT_NAME = 'storedirectoryscraper'


SPIDER_MODULES = ['storedirectoryscraper.spiders']
NEWSPIDER_MODULE = 'storedirectoryscraper.spiders'

ITEM_PIPELINES = {
    'storedirectoryscraper.pipelines.StoreDirectoryScraperPipeline': 300,
}

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'StoreDirectoryScraper (+http://www.yourdomain.com)'
